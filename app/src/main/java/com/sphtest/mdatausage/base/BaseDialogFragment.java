package com.sphtest.mdatausage.base;

import androidx.fragment.app.DialogFragment;

public class BaseDialogFragment extends DialogFragment {
    public BaseActivity getBaseActivity() {
        return (BaseActivity) getActivity();
    }
}
