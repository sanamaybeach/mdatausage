package com.sphtest.mdatausage.base.networking

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BaseErrorObject(
    var code: String? = null,
    var message: String? = null,
    var url: String? = null
) : Parcelable