package com.sphtest.mdatausage.base.networking

import com.google.gson.annotations.SerializedName

open class BaseResponse {
    @SerializedName("error")
    val errorObject: BaseErrorObject = BaseErrorObject()

    @SerializedName("success")
    val isSuccess: Boolean = false

    @SerializedName("help")
    private val help: String? = null

}
