package com.sphtest.mdatausage.base

abstract class ListItem {

    abstract val listType: Int

    companion object {
        val TYPE_GROUP = 0
        val TYPE_CHILD = 1
        val TYPE_LOADING = 2
    }
}
