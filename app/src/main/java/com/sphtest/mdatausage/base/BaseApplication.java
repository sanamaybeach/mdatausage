package com.sphtest.mdatausage.base;


import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.TypedValue;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AlertDialog;

import com.sphtest.mdatausage.R;
import com.sphtest.mdatausage.utils.LogPrinter;
import com.sphtest.mdatausage.utils.Utils;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class BaseApplication extends Application {
    public static final String TAG = BaseApplication.class.getSimpleName();

    private static BaseApplication mInstance;

    public static synchronized BaseApplication getInstance() {
        return mInstance;
    }

    public static Context getAppContext() {
        return mInstance.getApplicationContext();
    }

    private Activity mCurrentActivity = null;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        setupRealmUpdate();
    }

    private void setupRealmUpdate() {
        try {
            Realm.init(BaseApplication.this);
            RealmConfiguration config = new RealmConfiguration.Builder()
                    .name("mdatausage.realm")
                    .deleteRealmIfMigrationNeeded()
                    .build();

            Realm.setDefaultConfiguration(config);

        } catch (Exception e) {
            LogPrinter.INSTANCE.printException(e);
        }
    }

    public Activity getCurrentActivity(){
        return mCurrentActivity;
    }

    public void setCurrentActivity(Activity mCurrentActivity){
        this.mCurrentActivity = mCurrentActivity;
    }

    public static void notifyServerError() {
        final Handler handler;
        handler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message message) {
                // This is where you do your work in the UI thread.
                // Your worker tells you in the message what to do.
                try {
                    if (Utils.Companion.isNetworkConnected(mInstance.getApplicationContext())) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(mInstance.getCurrentActivity());
                        builder.setTitle(R.string.server_error_title)
                                .setMessage(R.string.server_error_desc)
                                .setCancelable(false)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                        dialog.dismiss();
                                        mInstance.mCurrentActivity.finish();
                                    }
                                });

                        final AlertDialog dialog = builder.create();
                        final Window dialogWindow = dialog.getWindow();
                        final WindowManager.LayoutParams dialogWindowAttributes = dialogWindow.getAttributes();

                        // Set fixed width (280dp) and WRAP_CONTENT height
                        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
                        lp.copyFrom(dialogWindowAttributes);
                        lp.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 280, mInstance.getApplicationContext().getResources().getDisplayMetrics());
                        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

                        // Set to TYPE_SYSTEM_ALERT so that the Service can display it
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            dialogWindow.setType(WindowManager.LayoutParams.TYPE_TOAST);
                        }
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            dialogWindow.setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
                        }
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                            dialogWindow.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                        }
                        dialogWindow.setAttributes(lp);


                        dialog.show();
                    }


                } catch (Exception e) {
                    LogPrinter.INSTANCE.printException(e);
                }
            }
        };
        handler.sendEmptyMessage(0);
    }

}

