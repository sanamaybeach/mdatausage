package com.sphtest.mdatausage.base

import androidx.annotation.NonNull

interface Lifecycle {
    interface View

    interface ViewModel {
        fun onViewResumed()
        fun onViewAttached(@NonNull viewCallback: Lifecycle.View)
        fun onViewDetached()
    }
}