package com.sphtest.mdatausage.base

import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity() {

    override fun onResume() {
        super.onResume()
        BaseApplication.getInstance()?.currentActivity = this
    }

    override fun onPause() {
        clearReferences()
        super.onPause()
    }

    override fun onDestroy() {
        clearReferences()
        super.onDestroy()
    }

    private fun clearReferences() {
        val currActivity = BaseApplication.getInstance().currentActivity
        if (this == currActivity)
            BaseApplication.getInstance().currentActivity = null
    }
}
