package com.sphtest.mdatausage.base;

public abstract class BaseMVVMActivity extends BaseActivity implements Lifecycle.View {
    protected abstract Lifecycle.ViewModel getViewModel();

    @Override
    protected void onStart() {
        super.onStart();
        getViewModel().onViewAttached(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getViewModel().onViewResumed();
    }

    @Override
    protected void onStop() {
        super.onStop();
        getViewModel().onViewDetached();
    }

}
