package com.sphtest.mdatausage.base;

import androidx.annotation.CallSuper;

import com.sphtest.mdatausage.base.networking.BaseResponse;
import com.sphtest.mdatausage.utils.NetworkConstants;

import io.reactivex.observers.DisposableMaybeObserver;

import static com.sphtest.mdatausage.utils.NetworkConstants.REQUEST_FAILED;
import static com.sphtest.mdatausage.utils.NetworkConstants.REQUEST_NONE;
import static com.sphtest.mdatausage.utils.NetworkConstants.REQUEST_RUNNING;
import static com.sphtest.mdatausage.utils.NetworkConstants.REQUEST_SUCCEEDED;

public abstract class NetworkViewModel {

    @NetworkConstants.RequestState
    private int requestState;
    private Throwable lastError;
    BaseResponse baseResponse;

    public abstract boolean isRequestingInformation();

    public NetworkViewModel() {
        this.requestState = REQUEST_NONE;
    }

    public @NetworkConstants.RequestState
    int getRequestState() {

        if (isRequestingInformation()) {
            return REQUEST_RUNNING;
        }

        return requestState;
    }

    public Throwable getLastError() {

        return lastError;
    }

    public BaseResponse getResponse() {
        return baseResponse;
    }

    protected class MaybeNetworkObserver<T extends BaseResponse> extends DisposableMaybeObserver<T> {

        @Override
        @CallSuper
        public void onSuccess(T value) {
            baseResponse = value;
            if (baseResponse.isSuccess()) {
                requestState = REQUEST_SUCCEEDED;

            } else {
                requestState = REQUEST_FAILED;
            }
        }

        @Override
        @CallSuper
        public void onError(Throwable e) {

            lastError = e;
            requestState = REQUEST_FAILED;
        }

        @Override
        public void onComplete() {

        }

    }
}
