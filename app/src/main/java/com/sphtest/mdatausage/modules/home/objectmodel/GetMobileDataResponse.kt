package com.sphtest.mdatausage.modules.home.objectmodel

import com.google.gson.annotations.SerializedName
import com.sphtest.mdatausage.base.networking.BaseResponse

class GetMobileDataResponse {
    class Request {
        var requestBody: HashMap<String, Any> = HashMap()
    }

    class Response : BaseResponse() {
        @SerializedName("result")
        val response: MobileDataResponseObject? = MobileDataResponseObject()

    }
}
