package com.sphtest.mdatausage.modules.home.objectmodel

import android.os.Parcelable
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
open class MobileRecordRealmObject(
    @PrimaryKey var _id: Int = 0,
    var year: Int? = null,
    var volume_of_mobile_data: Double? = null,
    var quarter_name: String? = null
) :  Parcelable, RealmObject()
