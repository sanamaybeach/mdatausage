package com.sphtest.mdatausage.modules.home.objectmodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FieldObject(
    var type: String? = null,
    var id: String? = null
) : Parcelable