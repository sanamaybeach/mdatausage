package com.sphtest.mdatausage.modules.home.networking

import com.sphtest.mdatausage.modules.home.objectmodel.GetMobileDataResponse
import com.sphtest.mdatausage.utils.singleton.RetrofitClient
import io.reactivex.Maybe
import io.reactivex.disposables.Disposable

class MobileDataApiService {
    private var mImageGalleryApi: MobileDataApi =
        RetrofitClient.getClient().create(MobileDataApi::class.java)
    var isRequestData: Boolean = false
        private set

    fun getMobileDataUsage(resource_id: String): Maybe<GetMobileDataResponse.Response> {
        return mImageGalleryApi.getMobileDataUsage(resource_id = resource_id)
            .doOnSubscribe { _: Disposable -> isRequestData = true }
            .doOnTerminate { isRequestData = false }
            .singleElement()
    }

}
