package com.sphtest.mdatausage.modules.home.objectmodel

import android.os.Parcelable
import com.sphtest.mdatausage.base.ListItem
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GroupTitleObject(
    var title: String
) : Parcelable, ListItem() {
    override val listType: Int
        get() = ListItem.TYPE_GROUP
}