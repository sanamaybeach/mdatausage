package com.sphtest.mdatausage.modules.home.contract

import com.sphtest.mdatausage.base.Lifecycle
import com.sphtest.mdatausage.modules.home.objectmodel.MobileDataResponseObject

interface MobileDataContract {
    interface View : Lifecycle.View {
        fun setMobileDataResult(data: MobileDataResponseObject?)
    }

    interface ViewModel : Lifecycle.ViewModel {
        fun getMobileDataResult(recordID: String)
    }
}
