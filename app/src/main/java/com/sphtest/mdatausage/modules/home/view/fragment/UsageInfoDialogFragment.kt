package com.sphtest.mdatausage.modules.home.view.fragment

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.sphtest.mdatausage.R
import com.sphtest.mdatausage.base.BaseDialogFragment
import com.sphtest.mdatausage.databinding.DialogFragmentUsageInfoBinding
import com.sphtest.mdatausage.modules.home.objectmodel.AnnualMobileDataObject
import com.sphtest.mdatausage.modules.home.view.adapter.QuarterDataUsageAdapter
import com.sphtest.mdatausage.utils.LogPrinter

class UsageInfoDialogFragment : BaseDialogFragment() {

    private var mWindow: Window? = null
    private var mContext: Context? = null

    private var mItemAdapter: QuarterDataUsageAdapter? = null
    private var mRecyclerView: RecyclerView? = null
    private var mBinding: DialogFragmentUsageInfoBinding? = null

    private var mAnnualData: AnnualMobileDataObject? = null
    private var mTitle: String = ""
    private var mCloseClickListener = View.OnClickListener { _ -> }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mContext = baseActivity


        mAnnualData =
            arguments?.getParcelable((DIALOGFRAGMENT_OBJECT_KEY)) ?: AnnualMobileDataObject(
                "0",
                0.0
            )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mWindow = dialog.window
        mWindow!!.requestFeature(Window.FEATURE_NO_TITLE)

        mBinding = DialogFragmentUsageInfoBinding.inflate(inflater, container, false)
        mTitle = getString(R.string.decrease_data_desc, mAnnualData?.year)

        mBinding?.title = mTitle
        mRecyclerView = mBinding?.rvQuarterData

        setupAdapter()
        setupOnClickListener()


        return mBinding!!.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        /** true - able to tap outside and cancel, false - otherwise  */
        dialog.setCanceledOnTouchOutside(true)

        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        return dialog
    }

    override fun onResume() {
        try {
            val width = (resources.displayMetrics.widthPixels * 0.9).toInt()
            val height = (resources.displayMetrics.heightPixels * 0.6).toInt()
            if (dialog != null) {
                dialog.window!!.setLayout(width, height)
            }

        } catch (e: Exception) {
            LogPrinter.printException(e)
        }

        super.onResume()
    }

    fun setCloseClickListener(closeClickListener: View.OnClickListener) {
        this.mCloseClickListener = closeClickListener
    }

    private fun setupOnClickListener() {
        mBinding?.btnOkay?.setOnClickListener { view ->
            mCloseClickListener.onClick(view)
            dismiss()
        }
    }

    private fun setupAdapter() {
        mContext?.let {
            val mLayoutManager = LinearLayoutManager(context)
            val mQuarterData = mAnnualData?.recordData ?: arrayListOf()
            mItemAdapter = QuarterDataUsageAdapter(mQuarterData)
            mRecyclerView?.layoutManager = mLayoutManager
            mRecyclerView?.adapter = mItemAdapter

            LogPrinter.print(TAG, "Size ${mQuarterData.size}")
        }
    }

    class Builder {
        private var mTitle: String = ""
        private var mAnnualData: AnnualMobileDataObject? = null
        private var mCloseClickListener = View.OnClickListener { _ -> }

        fun annualData(annualData: AnnualMobileDataObject): UsageInfoDialogFragment.Builder {
            this.mAnnualData = annualData
            return this
        }

        fun closeListener(listener: View.OnClickListener): UsageInfoDialogFragment.Builder {
            this.mCloseClickListener = listener
            return this
        }

        fun build(): UsageInfoDialogFragment {
            val bundle = Bundle()
            bundle.putParcelable(DIALOGFRAGMENT_OBJECT_KEY, mAnnualData)

            val dialogFragment = UsageInfoDialogFragment()
            dialogFragment.arguments = bundle
            dialogFragment.setCloseClickListener(mCloseClickListener)

            return dialogFragment
        }
    }

    companion object {
        const val DIALOGFRAGMENT_OBJECT_KEY = "DIALOGFRAGMENT_OBJECT_KEY"
        var TAG = UsageInfoDialogFragment::class.java.simpleName
    }

}