package com.sphtest.mdatausage.modules.home.networking

import com.sphtest.mdatausage.modules.home.objectmodel.GetMobileDataResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query


interface MobileDataApi {

    @GET("api/action/datastore_search")
    fun getMobileDataUsage(@Query("resource_id") resource_id: String): Observable<GetMobileDataResponse.Response>

}