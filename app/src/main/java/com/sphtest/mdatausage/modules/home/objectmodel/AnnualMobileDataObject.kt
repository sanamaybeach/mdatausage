package com.sphtest.mdatausage.modules.home.objectmodel

import android.os.Parcelable
import com.sphtest.mdatausage.base.ListItem
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AnnualMobileDataObject(
    var year: String,
    var dataUsage: Double,
    var isDecreaseValue: Boolean = false,
    var recordData: List<MobileRecordRealmObject> = arrayListOf()
) : Parcelable, ListItem() {
    override val listType: Int
        get() = ListItem.TYPE_CHILD
}