package com.sphtest.mdatausage.modules.home.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sphtest.mdatausage.base.ListItem
import com.sphtest.mdatausage.databinding.ItemDataRecordBinding
import com.sphtest.mdatausage.databinding.ItemGroupTitleBinding
import com.sphtest.mdatausage.modules.home.objectmodel.AnnualMobileDataObject
import com.sphtest.mdatausage.modules.home.objectmodel.GroupTitleObject

class DataUsageAdapter(
    private val mDataUsageList: ArrayList<ListItem>,
    private val mSelectDataUsageListener: SelectDataUsageListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG = DataUsageAdapter::class.java.simpleName

    override fun getItemCount(): Int {
        return mDataUsageList.size
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        lateinit var viewHolder: RecyclerView.ViewHolder
        when (viewType) {
            ListItem.TYPE_GROUP -> {
                val binding = ItemGroupTitleBinding
                    .inflate(layoutInflater, parent, false)
                viewHolder = TitleGroupViewHolder(binding)
            }
            else -> {
                val profileBinding =
                    ItemDataRecordBinding.inflate(layoutInflater, parent, false)
                viewHolder = DataUsageViewHolder(profileBinding)
            }

        }
        return viewHolder
    }

    override fun onBindViewHolder(
        holder: RecyclerView.ViewHolder,
        position: Int
    ) {
        when (holder.itemViewType) {
            ListItem.TYPE_GROUP -> {
                val mUsageHolder = holder as TitleGroupViewHolder
                val mTitle = mDataUsageList[position] as GroupTitleObject
                mUsageHolder.bind(mTitle)
            }
            else -> {
                val mUsageHolder = holder as DataUsageViewHolder
                val dataUsageItem = mDataUsageList[position] as AnnualMobileDataObject
                mUsageHolder.bind(dataUsageItem)

                mUsageHolder.dataRecordBinding.cardView.setOnClickListener {
                    mSelectDataUsageListener.selectDataUsage(dataUsageItem)
                }

                mUsageHolder.dataRecordBinding.imageView.setOnClickListener {
                    mSelectDataUsageListener.selectDecreaseDataUsage(dataUsageItem)
                }
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return mDataUsageList[position].listType
    }

    inner class DataUsageViewHolder(var dataRecordBinding: ItemDataRecordBinding) :
        RecyclerView.ViewHolder(dataRecordBinding.root) {

        fun bind(record: AnnualMobileDataObject) {
            dataRecordBinding.record = record
            dataRecordBinding.executePendingBindings()
        }
    }

    inner class TitleGroupViewHolder(
        var titleGroupBinding: ItemGroupTitleBinding
    ) : RecyclerView.ViewHolder(titleGroupBinding.root) {
        fun bind(mTitle: GroupTitleObject) {
            titleGroupBinding.title = mTitle.title
            titleGroupBinding.executePendingBindings()
        }
    }

    interface SelectDataUsageListener {
        fun selectDataUsage(record: AnnualMobileDataObject)
        fun selectDecreaseDataUsage(record: AnnualMobileDataObject)
    }
}

