package com.sphtest.mdatausage.modules.home.objectmodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MobileDataResponseObject(
    var resource_id: String? = null,
    var fields: ArrayList<FieldObject> = arrayListOf(),
    var records: ArrayList<RecordsObject> = arrayListOf(),
    var _links: LinkObject = LinkObject(),
    var limit: Int? = 0,
    var total: Int? = 0
) : Parcelable
