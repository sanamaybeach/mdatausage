package com.sphtest.mdatausage.modules.home.view.activity

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableBoolean
import androidx.recyclerview.widget.LinearLayoutManager
import com.sphtest.mdatausage.R
import com.sphtest.mdatausage.base.BaseMVVMActivity
import com.sphtest.mdatausage.base.Lifecycle
import com.sphtest.mdatausage.base.ListItem
import com.sphtest.mdatausage.databinding.ActivityMainBinding
import com.sphtest.mdatausage.modules.home.contract.MobileDataContract
import com.sphtest.mdatausage.modules.home.objectmodel.AnnualMobileDataObject
import com.sphtest.mdatausage.modules.home.objectmodel.GroupTitleObject
import com.sphtest.mdatausage.modules.home.objectmodel.MobileDataResponseObject
import com.sphtest.mdatausage.modules.home.objectmodel.MobileRecordRealmObject
import com.sphtest.mdatausage.modules.home.view.adapter.DataUsageAdapter
import com.sphtest.mdatausage.modules.home.view.fragment.UsageInfoDialogFragment
import com.sphtest.mdatausage.modules.home.viewmodel.MobileDataViewModel
import com.sphtest.mdatausage.utils.RecordRealmManager
import com.sphtest.mdatausage.utils.Utils

class MainActivity : BaseMVVMActivity(), MobileDataContract.View {
    val TAG = MainActivity::class.java.simpleName
    private var mViewModel: MobileDataContract.ViewModel? = null
    private lateinit var mContext: Context
    private var mBinding: ActivityMainBinding? = null
    private var mRecordID: String = ""
    private var mTitle: String = ""
    private var MIN_YEAR = 2008
    private var MAX_YEAR = 2018

    private var mDataUsageAdapter: DataUsageAdapter? = null
    private lateinit var mClickDataUsageHandler: DataUsageAdapter.SelectDataUsageListener
    private var mDataUsageList: ArrayList<ListItem> = arrayListOf()
    val isNoDataDisplay = ObservableBoolean(false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = DataBindingUtil.setContentView(this@MainActivity, R.layout.activity_main)
        mBinding?.view = this@MainActivity

        mViewModel = MobileDataViewModel()
        mContext = this@MainActivity
        mRecordID = getString(R.string.resource_key)
        mTitle = getString(R.string.title_annual_mobile_usage, MIN_YEAR.toString(), MAX_YEAR.toString())
        setupOnClickListener()
        setupAdapter()

        setupNoNetworkDisplay()
        initGetDataUsageRecord()
    }

    override fun getViewModel(): Lifecycle.ViewModel? {
        return mViewModel
    }

    override fun setMobileDataResult(data: MobileDataResponseObject?) {
        data?.records?.let { recordsList ->
            if (recordsList.size > 0) {
                // Save retrieved data in realm
                RecordRealmManager.saveMobileDataUsageList(recordsList)
            } else {
                RecordRealmManager.deleteFromRealm()
            }
            generateAnnualDataList()
        }
    }

    private fun getDataUsageRecord() {
        mViewModel?.getMobileDataResult(recordID = mRecordID)
    }

    /**
     * Setup Adapter
     * */
    private fun setupAdapter() {
        val layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        mDataUsageAdapter =
            DataUsageAdapter(mDataUsageList, mClickDataUsageHandler)

        mBinding?.let { mBinding ->
            mBinding.rvDataRecord.layoutManager = layoutManager
            mBinding.rvDataRecord.adapter = mDataUsageAdapter
        }
    }

    /**
     * Setup Click Listeners
     * */
    private fun setupOnClickListener() {
        mClickDataUsageHandler = object : DataUsageAdapter.SelectDataUsageListener {
            override fun selectDataUsage(record: AnnualMobileDataObject) {
                // Not yet implemented
            }

            override fun selectDecreaseDataUsage(record: AnnualMobileDataObject) {
                launchQuarterInfoDialog(record)
            }
        }
    }

    /**
     * Annual Data Generator
     * */
    private fun generateAnnualDataList() {

        // Retrieve data from database
        val retrieveDataList = RecordRealmManager.retrieveMobileDataUsageByYear(MIN_YEAR, MAX_YEAR)
        val annualMobileDataList = arrayListOf<AnnualMobileDataObject>()

        var year = MIN_YEAR
        while (year < MAX_YEAR + 1) {

            // Filter data by year
            val annualList = retrieveDataList.filter { it.year == year }

            // Total annual data
            val annualValue = annualList.sumByDouble { it.volume_of_mobile_data ?: 0.0 }

            // Create Annual Data Object
            val annualData = AnnualMobileDataObject(year.toString(), annualValue)
            annualData.recordData = annualList

            annualData.isDecreaseValue = isDecreaseValue(annualList)

            annualMobileDataList.add(annualData)
            year++
        }

        // Clear data display
        mDataUsageList.clear()
        mDataUsageList.add(GroupTitleObject(mTitle))
        mDataUsageList.addAll(annualMobileDataList)

        runOnUiThread {
            mDataUsageAdapter?.notifyDataSetChanged()
            setNoDataDisplay()
        }
    }

    /**
     * Launch Popup Quarter information
     * */
    private fun launchQuarterInfoDialog(
        annualData: AnnualMobileDataObject
    ) {
        val oldFrag = supportFragmentManager.findFragmentByTag(UsageInfoDialogFragment.TAG)
        val fragmentTransaction = supportFragmentManager.beginTransaction()

        if (oldFrag != null) {
            fragmentTransaction.remove(oldFrag)
        }

        val builder = UsageInfoDialogFragment.Builder()
            .closeListener(View.OnClickListener {
                // Not yet implemented
            })
            .annualData(annualData)


        val quarterInfoDialogFragment = builder.build()
        quarterInfoDialogFragment.isCancelable = true
        quarterInfoDialogFragment.show(fragmentTransaction, UsageInfoDialogFragment.TAG)
    }

    private fun isDecreaseValue(dataList: List<MobileRecordRealmObject>): Boolean {
        var isDecreaseValue = false
        dataList.forEachIndexed { index, item ->
            // Check if current data is less then previous data
            if (index > 0 && item.volume_of_mobile_data ?: 0.0 < dataList[index - 1].volume_of_mobile_data ?: 0.0) {
                isDecreaseValue = true
            }
        }

        return isDecreaseValue
    }

    /**
     * Retrieve Data Usage Record
     * */
    private fun initGetDataUsageRecord() {
        val isCacheDataExists = RecordRealmManager.dataExists()
        if (Utils.isNetworkConnected(mContext) && !isCacheDataExists) {
            getDataUsageRecord()
            displayNoNetwork(false)
        } else {
            if (isCacheDataExists) {
                generateAnnualDataList()
                displayNoNetwork(false)
            } else {
                displayNoNetwork(true)
            }
        }
    }


    /**
     * Setup no data display
     * */
    private fun setNoDataDisplay() {
        if (mDataUsageList.size > 1) {
            isNoDataDisplay.set(false)
        } else {
            isNoDataDisplay.set(true)
        }
    }

    /**
     * Setup no network display
     * */
    private fun setupNoNetworkDisplay() {
        mBinding?.layoutNoInternet?.btnRetry?.setOnClickListener {
            initGetDataUsageRecord()
        }
    }

    private fun displayNoNetwork(isDisplay:Boolean) {
        val noNetworkVisibility = if (isDisplay) {
            View.VISIBLE
        } else {
            View.GONE
        }
        runOnUiThread {
            mBinding?.layoutNoInternet?.root?.visibility = noNetworkVisibility
        }
    }

}
