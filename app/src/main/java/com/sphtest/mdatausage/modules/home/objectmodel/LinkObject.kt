package com.sphtest.mdatausage.modules.home.objectmodel

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LinkObject(
    var start: String? = null,
    var next: String? = null
) : Parcelable
