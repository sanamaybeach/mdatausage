package com.sphtest.mdatausage.modules.home.objectmodel

import android.os.Parcelable
import com.sphtest.mdatausage.base.ListItem
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RecordsObject(
    var _id: Int? = null,
    var volume_of_mobile_data: String? = null,
    var quarter: String? = null
) : Parcelable, ListItem() {
    override val listType: Int
        get() = ListItem.TYPE_CHILD


}
