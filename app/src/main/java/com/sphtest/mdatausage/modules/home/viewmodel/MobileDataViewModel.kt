package com.sphtest.mdatausage.modules.home.viewmodel

import com.sphtest.mdatausage.base.Lifecycle
import com.sphtest.mdatausage.base.NetworkViewModel
import com.sphtest.mdatausage.modules.home.contract.MobileDataContract
import com.sphtest.mdatausage.modules.home.networking.MobileDataApiService
import com.sphtest.mdatausage.modules.home.objectmodel.GetMobileDataResponse
import com.sphtest.mdatausage.modules.home.objectmodel.MobileDataResponseObject
import com.sphtest.mdatausage.utils.LogPrinter
import com.sphtest.mdatausage.utils.NetworkConstants
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MobileDataViewModel : NetworkViewModel(), MobileDataContract.ViewModel {
    private var mViewCallback: MobileDataContract.View? = null
    private var mApiService: MobileDataApiService = MobileDataApiService()

    override fun onViewResumed() {
        @NetworkConstants.RequestState val requestState = getRequestState()
        if (requestState == NetworkConstants.REQUEST_SUCCEEDED) {
            /** if this returns runtime error, then delete this away  */
        } else if (requestState == NetworkConstants.REQUEST_FAILED) {
            getResultError(getLastError())
        }
    }

    override fun onViewAttached(viewCallback: Lifecycle.View) {
        mViewCallback = viewCallback as MobileDataContract.View
    }

    override fun onViewDetached() {
        mViewCallback = null
    }

    override fun isRequestingInformation(): Boolean {
        return mApiService.isRequestData
    }

    private fun getResultError(e: Throwable?) {
        if (mViewCallback != null) {
            LogPrinter.printThrowable(e)
        }
    }

    override fun getMobileDataResult(key: String) {
        mApiService.getMobileDataUsage(key)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(GetMobileDataResponseObserver())
    }

    private fun setMobileDataResultSuccessful(results: MobileDataResponseObject?) {
        mViewCallback?.setMobileDataResult(results)
    }

    private inner class GetMobileDataResponseObserver :
        NetworkViewModel.MaybeNetworkObserver<GetMobileDataResponse.Response>() {

        override fun onSuccess(@io.reactivex.annotations.NonNull response: GetMobileDataResponse.Response) {
            super.onSuccess(response)
            setMobileDataResultSuccessful(response.response)

        }

        override fun onError(@io.reactivex.annotations.NonNull e: Throwable) {
            super.onError(e)
            getResultError(e)
        }

        override fun onComplete() {

        }
    }
}
