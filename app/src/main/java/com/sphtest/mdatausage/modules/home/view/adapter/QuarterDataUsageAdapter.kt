package com.sphtest.mdatausage.modules.home.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sphtest.mdatausage.databinding.ItemDataRecordQuarterBinding
import com.sphtest.mdatausage.modules.home.objectmodel.MobileRecordRealmObject

class QuarterDataUsageAdapter(
    private val mQuarterDataUsageList: List<MobileRecordRealmObject>
) : RecyclerView.Adapter<QuarterDataUsageAdapter.DataUsageViewHolder>() {

    companion object {
        val TAG = QuarterDataUsageAdapter::class.java.simpleName
    }

    override fun getItemCount(): Int {
        return mQuarterDataUsageList.size
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DataUsageViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)
        val profileBinding =
            ItemDataRecordQuarterBinding.inflate(layoutInflater, parent, false)
        return DataUsageViewHolder(profileBinding)
    }

    override fun onBindViewHolder(
        holder: DataUsageViewHolder,
        position: Int
    ) {
        val dataUsageItem = mQuarterDataUsageList[position] as MobileRecordRealmObject
        holder.bind(dataUsageItem)
    }

    inner class DataUsageViewHolder(var dataRecordBinding: ItemDataRecordQuarterBinding) :
        RecyclerView.ViewHolder(dataRecordBinding.root) {

        fun bind(record: MobileRecordRealmObject) {
            dataRecordBinding.record = record
            dataRecordBinding.executePendingBindings()
        }
    }
}

