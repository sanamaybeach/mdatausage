package com.sphtest.mdatausage.utils

import com.sphtest.mdatausage.modules.home.objectmodel.MobileRecordRealmObject
import com.sphtest.mdatausage.modules.home.objectmodel.RecordsObject
import io.realm.Realm
import java.util.ArrayList

object RecordRealmManager {

    fun saveMobileDataUsageList(recordList: ArrayList<RecordsObject>) {
        var realm: Realm? = null
        try {
            realm = Realm.getDefaultInstance()

            for (record in recordList) {
                val recordItem = MobileRecordRealmObject()
                val queryData = record.quarter?.split("-") ?: arrayListOf()
                recordItem._id = record._id ?: 0

                recordItem.volume_of_mobile_data = record.volume_of_mobile_data?.toDoubleOrNull()
                if (queryData.size == 2) {
                    recordItem.quarter_name = queryData[1]
                    recordItem.year = queryData[0].toIntOrNull()
                }
                realm?.executeTransaction { it.insertOrUpdate(recordItem) }
            }
        } catch (e: Exception) {
            LogPrinter.printException(e)
        } finally {
            realm?.close()
        }
    }

    fun retrieveMobileDataUsageByYear(minYear: Int, maxYear:Int): MutableList<MobileRecordRealmObject> {
        var realm: Realm? = null
        var result = mutableListOf<MobileRecordRealmObject>()
        try {
            realm = Realm.getDefaultInstance()

            result =  realm.copyFromRealm(
                realm.where(MobileRecordRealmObject::class.java)
                    .between("year", minYear,maxYear)
                    .findAll())
        } catch (e: Exception) {
            LogPrinter.printException(e)
        } finally {
            realm?.close()
        }
        return result
    }

    fun dataExists(): Boolean {
        var realm: Realm? = null
        var hasData = false
        try {
            realm = Realm.getDefaultInstance()
            hasData = realm.where(MobileRecordRealmObject::class.java).findAll().count() > 0
        } catch (e: Exception) {
            LogPrinter.printException(e)
        } finally {
            realm?.close()
        }

        return hasData
    }

    fun deleteFromRealm() {
        val realm = Realm.getDefaultInstance()

        realm.executeTransactionAsync { transactionRealm ->
            transactionRealm.where(MobileRecordRealmObject::class.java)
                .findAll()
                .deleteAllFromRealm()
        }

        realm.close()
    }
}