package com.sphtest.mdatausage.utils

import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast
import com.sphtest.mdatausage.R

class Utils {

    companion object {
        fun displayToast(context: Context?, message: String) {
            if (context != null) {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
            }
        }

        fun isNetworkConnected(context: Context): Boolean {
            var isConnected = false

            try {
                val cm =
                    context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

                val activeNetwork = cm.activeNetworkInfo
                isConnected = activeNetwork != null && activeNetwork.isConnected
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return isConnected

        }
    }
}