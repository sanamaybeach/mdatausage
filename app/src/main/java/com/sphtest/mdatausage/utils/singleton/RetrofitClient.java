package com.sphtest.mdatausage.utils.singleton;

import com.sphtest.mdatausage.BuildConfig;
import com.sphtest.mdatausage.utils.LogPrinter;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static Retrofit mInstance = null;
    private final static String TAG = RetrofitClient.class.getSimpleName();

    public static Retrofit getClient() {

        synchronized (RetrofitClient.class) {
            if (mInstance == null) {

                /** Define the interceptor, add authentication header(s)
                 *      header - replace the value with similar key
                 *      addHeader - add another value even with similar key */
                Interceptor interceptor = new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Interceptor.Chain chain) throws IOException {
                        okhttp3.Request original = chain.request();

                        /** Request customization: add request headers */
                        okhttp3.Request.Builder requestBuilder = original.newBuilder();
//                                .header("Authorization", "auth-value"); // <-- this is the important line

                        okhttp3.Request request = requestBuilder.build();

                        return chain.proceed(request);
                    }
                };

                /** Printing out the http request body in logcat */
                HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor()
                        .setLevel(HttpLoggingInterceptor.Level.BODY);

                // Error handling interceptor
                Interceptor errorInterceptopr = new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        okhttp3.Response response = null;
                        try {
                            response = chain.proceed(request);

                            if (response.code() ==500) {
                                //TODO: Add Error Handler to deal with the issues the way you need to

                                return response;
                            }
                        } catch (Exception e) {
                            LogPrinter.INSTANCE.printException(e);
                        }

                        return response;
                    }
                };

                /** Add the interceptor to OkHttpClient. Set timeout to 0 to disable timeout */
                if (BuildConfig.BUILD_TYPE.contentEquals("debug")) {

                    OkHttpClient okHttpClient = new OkHttpClient.Builder()
                            .connectTimeout(0, TimeUnit.SECONDS)  // connect timeout
                            .writeTimeout(0, TimeUnit.SECONDS)
                            .readTimeout(0, TimeUnit.SECONDS)      // socket timeout
                            .addInterceptor(interceptor)
                            .addInterceptor(httpLoggingInterceptor)
                            .addInterceptor(errorInterceptopr)
                            .build();


                    mInstance = new Retrofit.Builder()
                            .baseUrl(BuildConfig.BASEURL)
                            .client(okHttpClient)
                            .addConverterFactory(GsonConverterFactory.create())
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                            .build();
                }else{
                    OkHttpClient okHttpClient = new OkHttpClient.Builder()
                            .connectTimeout(0, TimeUnit.SECONDS)  // connect timeout
                            .writeTimeout(0, TimeUnit.SECONDS)
                            .readTimeout(0, TimeUnit.SECONDS)      // socket timeout
                            .addInterceptor(interceptor)
//                            .addInterceptor(httpLoggingInterceptor)
                            .addInterceptor(errorInterceptopr)
                            .build();


                    mInstance = new Retrofit.Builder()
                            .baseUrl(BuildConfig.BASEURL)
                            .client(okHttpClient)
                            .addConverterFactory(GsonConverterFactory.create())
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                            .build();
                }


            }
            return mInstance;
        }
    }
}
