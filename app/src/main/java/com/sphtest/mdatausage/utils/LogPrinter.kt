package com.sphtest.mdatausage.utils

import android.util.Log
import com.sphtest.mdatausage.BuildConfig


object LogPrinter {
    fun print(tag: String, text: String) {

        if (BuildConfig.BUILD_TYPE.contentEquals("debug")) {
            Log.i(tag, "PRINT:$text")
        }
    }

    fun printException(e: Exception) {
        e.printStackTrace()
    }

    fun printThrowable(throwable: Throwable?) {
        // Check if null as sometimes it causes NullPointerException
        throwable?.printStackTrace()
    }

    fun printError(tag: String, string: String) {
        if (BuildConfig.BUILD_TYPE.contentEquals("debug")) {
            Log.e(tag, "ERROR:$string")
        }
    }

    fun printDebug(tag: String, string: String) {
        if (BuildConfig.BUILD_TYPE.contentEquals("debug")) {
            Log.d(tag, "DEBUG:$string")
        }
    }
}
